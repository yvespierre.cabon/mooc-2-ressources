
## Objectifs
Dédramatiser le codage en présentant quelques concepts fondamentaux de façon ludique
## Pré-requis à cette activité
niveau collège
## Durée de l'activité
4 heures
## Exercices cibles
?
## Description du déroulement de l'activité
Montrer un exemple visuel attrayant d'emploi de turtle (fractales par exemple) puis 2 * 2 heures (modules 1 et 2 puis modules 3 et 4)
## Anticipation des difficultés des élèves
notion de boucle for imbriquées
## Gestion de l'hétérogénéïté
Autonomie pour les plus à l'aise, afin de se concentrer sur ceux ayant des difficultés
